require "rspec"
require_relative "../lib/vending_machine"

describe VendingMachine do
  let(:mars) { Product.new("Mars", 100) }
  let(:stocked_products) { Stock.new([mars]) }
  let(:one_pound) { Coin.new(100) }

  it "can load products" do
    machine = VendingMachine.new(stocked_products)

    expect(machine.stocked_products).to include("Mars")
  end

  it "can load money" do
    machine = VendingMachine.new(stocked_products, CoinSlot.new([one_pound]))

    expect(machine.total_money).to eq(100)
  end

  it "can take money" do
    fifty_pence = Coin.new(50)

    machine = VendingMachine.new(stocked_products, CoinSlot.new([one_pound]))
    machine.load_money(fifty_pence)

    expect(machine.total_money).to eq 150
  end

  it "can add new products" do
    machine = VendingMachine.new(stocked_products, CoinSlot.new([one_pound]))
    new_product = Product.new("Doritos", 100)
    machine.add_products(new_product)

    expect(machine.stocked_products).to include("Doritos")
  end

  it "keeps track of money inserted" do
    machine = VendingMachine.new(stocked_products, CoinSlot.new([one_pound]))
    fifty_pence = Coin.new(50)
    machine.put_money(fifty_pence)
    expect(machine.total_money_inserted).to eq(50)

    machine.put_money(fifty_pence)

    expect(machine.total_money_inserted).to eq(100)
  end

  it "dispenses no product if not enough money if put into it" do
    machine = VendingMachine.new(stocked_products, CoinSlot.new([one_pound]))

    machine.pick_product("Mars")

    expect(machine.dispense).to eq("Not enough money")
  end

  it "dispenses single product when money is exact" do
    machine = VendingMachine.new(stocked_products, CoinSlot.new([one_pound]))

    machine.put_money(Coin.new(100))
    machine.pick_product("Mars")

    expect(machine.dispense.first.name).to include("Mars")
  end

  it "dispenses single product and change if more money is inserted" do
    machine = VendingMachine.new(stocked_products, CoinSlot.new([one_pound]))

    machine.put_money(Coin.new(100))
    machine.put_money(Coin.new(100))
    expect(machine.total_money_inserted).to eq(200)
    machine.pick_product("Mars")

    dispense = machine.dispense
    expect(dispense.first.name).to include("Mars")
    expect(dispense.last.amount).to eq(100)
  end
end
