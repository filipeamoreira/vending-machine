class Coin
  VALID_COINS = [1, 2, 5, 10, 20, 50, 100, 200]

  attr_reader :amount

  def initialize(amount)
    raise "Coin not recognised" unless VALID_COINS.include?(amount)
    @amount = amount
  end
end
