require_relative 'product'
require_relative 'stock'
require_relative 'coin_slot'
require_relative "coin"

class VendingMachine
  def initialize(stock=Stock.new, machine_slot=CoinSlot.new)
    @stock = stock
    @machine_slot = machine_slot
    @money_inserted = CoinSlot.new
  end

  def add_products(products)
    @stock.add_stock(Array(products))
  end

  def add_change(change)
    @machine_slot.add(change)
  end

  def put_money(change)
    @money_inserted.add(change)
  end

  def stocked_products
    @stock.all
  end

  def total_money
    @machine_slot.total
  end

  def total_money_inserted
    @money_inserted.total
  end

  def load_money(money)
    @machine_slot.add(money)
  end


  def pick_product(product_name)
    @product = @stock.dispense(product_name)
  end

  def dispense
    # Exact money inserted
    if total_money_inserted == @product.price
      dispense = [dispense_selected_product]
      reset_product
    # Need to return change
    elsif total_money_inserted > @product.price
      dispense = [dispense_selected_product, dispense_change]
      reset_product
    elsif total_money_inserted < @product.price
      dispense = "Not enough money"
    end
    return dispense
  end

  def reset_product
    @product = nil
  end

  def dispense_selected_product
    dispensed_product = @product
    @machine_slot.add(@money_inserted.to_a)
    return dispensed_product
  end

  def dispense_change
    change_given = total_money_inserted - @product.price
    @machine_slot.refund(change_given)
  end
end
