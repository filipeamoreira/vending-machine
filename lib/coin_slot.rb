class CoinSlot

  def initialize(coins=[])
    @coins = coins
  end

  def add(coins)
    @coins << coins
  end

  def total
    # Making sure we get a number
    @coins.map(&:amount).reduce(0, :+)
  end

  def refund(change)
    found = @coins.index {|coin| coin.amount == change }
    @coins.delete_at(found)
  end

  def to_a
    @coins.to_a
  end
end
