class Stock
  attr_accessor :stock

  def initialize(products=[])
    @products = products
  end

  def add_stock(products)
    @products << products
  end

  def all
    @products.flatten.map(&:name)
  end

  def dispense(product_name)
    found = @products.index {|product| product.name == product_name }

    raise "#{product_name} not available at the moment" if !found
    @products.delete_at(found)
  end
end
